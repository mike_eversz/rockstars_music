# Rockstars Music

This repo contains the Rockstars Music API. 

## Pre-requisites
* Git
* Python3.9
* Docker
* Docker-compose


## Setup
#### Install
* The [Dockerfile](Dockerfile) sets up the pipenv environment, and installs the dependencies
* The [docker-compose.yml](docker-compose.yml) defines the 'webapp' and 'PostgreSQL database' Docker services and how it should be built

Installation steps: 

```
git clone git@bitbucket.org:mike_eversz/rockstars_music.git  

cp app/main/example.env app/main/.env
# Change the configuration values in .env file if you wish.

docker-compose up -d
```

#### Fill the database
* Filling the database is not fully automated. This is how I did it:
    * Download *artists.json* and *songs.json* files into a folder named 'data' in the root of the project.
    * Create database tables: *temp_artist* and *temp_song*, with both a column *json* of type *json*.
    * Use [fill_database.py](app/main/database/fill_database.py) script to put json data into temp tables.
    * Create database tables: *artist* and *song* and fill them from the temp tables with SQL.
    * Add constraints to the *artist* and *song* tables using PgAdmin UI. 
    
## Tests
Run the test using this command:
``
pytest app/tests
``