import os

from flask import Blueprint
from flask_restx import Api

from .main.controllers.artist_controller import api as artist_ns
from .main.controllers.song_controller import api as song_ns

# Instantiate Flask Api using a blueprint
blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(blueprint,
          title=os.getenv('API_NAME'),
          version=os.getenv('API_VERSION'),
          description=os.getenv('API_DESCRIPTION'))

# Add namespaces to the api which can be reused or updated in different API versions
api.add_namespace(artist_ns, path='/artists')
api.add_namespace(song_ns, path='/songs')
