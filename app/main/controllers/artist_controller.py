from flask import request
from flask_restx import Resource

from app.main.services import artist_service
from app.main.util.dto.artist_dto import ArtistDto

api = ArtistDto.api


@api.route('/')
class ArtistCollection(Resource):

    @api.marshal_with(ArtistDto.response_artist, as_list=True)
    def get(self):
        """Returns list of artists."""
        return artist_service.get_all_artists()

    @api.expect(ArtistDto.request_artist, validate=True)
    @api.marshal_with(ArtistDto.response_artist, code=201)
    @api.response(409, 'Artist already exists.')
    def post(self):
        """Creates a new artist."""
        artist = artist_service.create_artist(request.json)
        return artist, 201


@api.route('/<int:id>')
@api.response(404, 'Artist not found.')
class ArtistItem(Resource):

    @api.marshal_with(ArtistDto.response_artist)
    def get(self, id):
        """Returns artist by id."""
        return artist_service.get_artist(id)

    @api.marshal_with(ArtistDto.response_artist)
    @api.expect(ArtistDto.request_artist, validate=True)
    def put(self, id):
        """Updates artist by id."""
        return artist_service.update_artist(id, request.json)

    @api.response(204, 'Artist successfully deleted.')
    def delete(self, id):
        """Deletes artist by id."""
        artist_service.delete_artist(id)
        return '', 204
