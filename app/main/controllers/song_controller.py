from flask import request
from flask_restx import Resource

from app.main.services import song_service
from app.main.util.dto.song_dto import SongDto

api = SongDto.api


@api.route('/')
class SongCollection(Resource):

    @api.marshal_with(SongDto.response_song, as_list=True)
    def get(self):
        """Returns list of songs."""
        return song_service.get_all_songs()

    @api.expect(SongDto.request_song, validate=True)
    @api.marshal_with(SongDto.response_song, code=201)
    @api.response(409, 'Song already exists.')
    @api.response(422, 'No artist exists for the given artist_id')
    def post(self):
        """Creates a new song."""
        song = song_service.create_song(request.json)
        return song, 201


@api.route('/<int:id>')
@api.response(404, 'Song not found.')
class SongItem(Resource):

    @api.marshal_with(SongDto.response_song)
    def get(self, id):
        """Returns song by id."""
        return song_service.get_song(id)

    @api.marshal_with(SongDto.response_song)
    @api.expect(SongDto.request_song, validate=True)
    def put(self, id):
        """Updates song by id."""
        return song_service.update_song(id, request.json)

    @api.response(204, 'Artist successfully deleted.')
    def delete(self, id):
        """Deletes song by id."""
        song_service.delete_song(id)
        return '', 204
