import os

import psycopg2
from dotenv import load_dotenv
load_dotenv()

# Create connection to Postgresql database
# TODO: Create singleton design pattern for database connection
db_connection = psycopg2.connect(host=os.getenv('POSTGRES_HOST'),
                                 database=os.getenv('POSTGRES_DB'),
                                 user=os.getenv('POSTGRES_USER'),
                                 password=os.getenv('POSTGRES_PASSWORD'))

db_connection.autocommit = True
