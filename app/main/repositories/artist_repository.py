import werkzeug
from psycopg2._psycopg import Error

from app.main.repositories.repository_helper import RepositoryHelper

ARTIST_TABLE = 'artist'


class ArtistRepository:
    @staticmethod
    def insert_artist(data):
        try:
            artist_id = RepositoryHelper.create_resource(data, ARTIST_TABLE)
            data['id'] = artist_id
            return data
        except Error as error:
            handle_artist_exceptions(error)

    @staticmethod
    def select_all_artists():
        return RepositoryHelper.select_all_resources(ARTIST_TABLE)

    @staticmethod
    def select_artist(id):
        return RepositoryHelper.select_resource(id, ARTIST_TABLE)

    @staticmethod
    def update_artist(id, data):
        try:
            return RepositoryHelper.update_resource(id, data, ARTIST_TABLE)
        except Error as error:
            handle_artist_exceptions(error)

    @staticmethod
    def delete_artist(id):
        return RepositoryHelper.delete_resource(id, ARTIST_TABLE)


def handle_artist_exceptions(error):
    # PostgreSQL error code 23505 means UniqueViolation
    if error.pgcode == '23505':
        raise werkzeug.exceptions.Conflict('An artist with the given name already exists.')
    else:
        raise werkzeug.exceptions.InternalServerError
