import json

import werkzeug
from psycopg2 import extras

from app.main.repositories import db_connection
from app.main.util.database import sql_queries


class RepositoryHelper:
    @staticmethod
    def create_resource(data, db_table):
        with db_connection.cursor(cursor_factory=extras.RealDictCursor) as cursor:
            # Create placeholders for the values in the sql query
            values_placeholder = '%s, ' * len(data.keys())
            # Strip last comma and space
            values_placeholder = values_placeholder[:-2]

            sql_query = sql_queries.INSERT.format(db_table, ', '.join(data.keys()), values_placeholder)
            params = (*data.values(),)
            cursor.execute(sql_query, params)

            # Fetch the id of the new resource and return it
            resource = json.loads(json.dumps(cursor.fetchone()))
            return resource.get('id')

    @staticmethod
    def select_resource(id, db_table):
        with db_connection.cursor(cursor_factory=extras.RealDictCursor) as cursor:
            sql_query = sql_queries.SELECT_BY_ID.format(db_table)
            cursor.execute(sql_query, {'id': id})
            resource = json.loads(json.dumps(cursor.fetchone()))

            if resource is None:
                raise werkzeug.exceptions.NotFound

            return resource

    @staticmethod
    def select_all_resources(db_table):
        with db_connection.cursor(cursor_factory=extras.RealDictCursor) as cursor:
            sql_query = sql_queries.SELECT_ALL.format(db_table)
            cursor.execute(sql_query, None)
            resources = json.loads(json.dumps(cursor.fetchall()))
            return resources

    @staticmethod
    def update_resource(id, data, db_table):
        with db_connection.cursor(cursor_factory=extras.RealDictCursor) as cursor:
            sql_query = sql_queries.UPDATE_BY_ID.format(db_table, ' = %s, '.join(data.keys()) + ' = %s')
            params = (*data.values(), id)
            cursor.execute(sql_query, params)
            resource = json.loads(json.dumps(cursor.fetchone()))

            if resource is None:
                raise werkzeug.exceptions.NotFound

            return resource

    @staticmethod
    def delete_resource(id, db_table):
        with db_connection.cursor(cursor_factory=extras.RealDictCursor) as cursor:
            sql_query = sql_queries.DELETE_BY_ID.format(db_table)
            cursor.execute(sql_query, {'id': id})

            if cursor.rowcount == 0:
                raise werkzeug.exceptions.NotFound
