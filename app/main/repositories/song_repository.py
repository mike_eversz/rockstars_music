import werkzeug
from psycopg2._psycopg import Error

from app.main.repositories.repository_helper import RepositoryHelper

SONG_TABLE = 'song'


class SongRepository:
    @staticmethod
    def insert_song(data):
        try:
            song_id = RepositoryHelper.create_resource(data, SONG_TABLE)
            data['id'] = song_id
            return data
        except Error as error:
            handle_song_exceptions(error)

    @staticmethod
    def select_all_songs():
        return RepositoryHelper.select_all_resources(SONG_TABLE)

    @staticmethod
    def select_song(id):
        return RepositoryHelper.select_resource(id, SONG_TABLE)

    @staticmethod
    def update_song(id, data):
        try:
            return RepositoryHelper.update_resource(id, data, SONG_TABLE)
        except Error as error:
            handle_song_exceptions(error)

    @staticmethod
    def delete_song(id):
        return RepositoryHelper.delete_resource(id, SONG_TABLE)


def handle_song_exceptions(error):
    # PostgreSQL error code 23505 means unique violation
    if error.pgcode == '23505':
        raise werkzeug.exceptions.Conflict('A song with the given spotify_id already exists.')
    # PostgreSQL error code 23503 means foreign key violation
    elif error.pgcode == '23503':
        raise werkzeug.exceptions.UnprocessableEntity('No artist exists for the given artist_id.')
    else:
        raise werkzeug.exceptions.InternalServerError
