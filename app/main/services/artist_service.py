from app.main.repositories.artist_repository import ArtistRepository


def create_artist(data):
    return ArtistRepository.insert_artist(data)


def get_all_artists():
    return ArtistRepository.select_all_artists()


def get_artist(id):
    return ArtistRepository.select_artist(id)


def update_artist(id, data):
    return ArtistRepository.update_artist(id, data)


def delete_artist(id):
    ArtistRepository.delete_artist(id)
