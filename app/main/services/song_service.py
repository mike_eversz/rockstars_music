from app.main.repositories.song_repository import SongRepository


def create_song(data):
    return SongRepository.insert_song(data)


def get_all_songs():
    return SongRepository.select_all_songs()


def get_song(id):
    return SongRepository.select_song(id)


def update_song(id, data):
    return SongRepository.update_song(id, data)


def delete_song(id):
    SongRepository.delete_song(id)
