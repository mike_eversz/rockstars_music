import json
import os
import psycopg2
from dotenv import load_dotenv

load_dotenv()
json_path = 'data/artist.json'
table_name = 'temp_artist'

with open(json_path) as json_data:
    # Load the data
    record_list = json.load(json_data)
    print(record_list)

    # Extract the column names
    columns = [column.lower() for column in record_list[0].keys()]

    # Declare SQL query string
    record_list = [json.dumps(record).replace("'", "''") for record in record_list]
    values = ["'{0}'".format(record) for record in record_list]
    query = "INSERT INTO {} (json) VALUES ({});".format(table_name, '), ('.join(values))

with psycopg2.connect(host=os.getenv('POSTGRES_HOST'),
                      database=os.getenv('POSTGRES_DB'),
                      user=os.getenv('POSTGRES_USER'),
                      password=os.getenv('POSTGRES_PASSWORD')) as connection:
    with connection.cursor() as cursor:
        cursor.execute(query)
