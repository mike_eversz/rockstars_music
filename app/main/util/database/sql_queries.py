SELECT_ALL = "SELECT * " \
             "FROM {}"

SELECT_BY_ID = "SELECT * " \
               "FROM {} " \
               "WHERE id =  %(id)s"

DELETE_BY_ID = "DELETE " \
               "FROM {} " \
               "WHERE id = %(id)s"

UPDATE_BY_ID = "UPDATE {} " \
               "SET {} " \
               "WHERE id = %s " \
               "RETURNING *"

INSERT = "INSERT INTO {} " \
         "({}) VALUES ({}) " \
         "RETURNING id"


