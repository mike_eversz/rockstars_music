from flask_restx import Namespace, fields


class ArtistDto:
    api = Namespace('artist', description='artist related operations')
    request_artist = api.model('request_artist', {
        'name': fields.String(required=True, example='AC/DC', description='The name of the artist.')
    })

    response_artist = api.clone('response_artist', request_artist, {
        'id': fields.Integer(required=True, example=1, description='The id of the artist.')
    })
