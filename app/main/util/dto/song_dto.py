from flask_restx import Namespace, fields
import datetime

CURRENT_YEAR = datetime.datetime.now().year


class SongDto:
    api = Namespace('song', description='song related operations')
    request_song = api.model('request_song', {
        'artist_id': fields.Integer(required=True, example=6, description='The id of the artist'),
        'name': fields.String(required=True, example='Visions', description='The name of the song'),
        'publication_year': fields.Integer(required=True, min=0, max=CURRENT_YEAR, example=2007,
                                           description='The year the song is published.'),
        'shortname': fields.String(required=True, example='visions', description='The shortname of the song'),
        'bpm': fields.Integer(required=True, example=195, description='The beats per minute of the song.'),
        'duration': fields.Integer(required=True, example=180462,
                                   description='The duration of the song in milliseconds. '),
        'genre': fields.String(required=True, example='Metal', description='The genre of the song.'),
        'spotify_id': fields.String(required=False, max_length=22, example='693ql9D8zy0t7kEwfDP5vQ',
                                    description='The Spotify id of the song. Maximum of 22 characters'),
        'album': fields.String(required=False, example='Abnormality Demo',
                               description='The album that contains this song.')
    })

    response_song = api.clone('response_song', request_song, {
        'id': fields.Integer(required=True, example=11, description='The id of the song.'),
    })

