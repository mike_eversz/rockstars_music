import json
import unittest
import uuid

from server import app


class TestArtistEndpoint(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestArtistEndpoint, self).__init__(*args, **kwargs)
        # TODO: Mock database and make cleaner (DRY)!

        self.app = app.test_client()
        _random_name = str(uuid.uuid1())
        self.test_artist = {'name': _random_name}

    def test_create_artist(self):
        # Normal use case
        response = self.app.post('/api/artists/', data=json.dumps(self.test_artist), content_type='application/json')
        self.assertEqual(201, response.status_code)

        # Abnormal use case, expect 409 because of duplicate artist name
        response = self.app.post('/api/artists/', data=json.dumps(self.test_artist), content_type='application/json')
        self.assertEqual(409, response.status_code)

    def test_get_all_artists(self):
        # Normal use case
        response = self.app.get('/api/artists/')
        self.assertIsInstance(response.json, list)
        self.assertEqual(200, response.status_code)

    def test_get_artist(self):
        # Add test artist and catch the given id
        creation_response = self.app.post('/api/artists/', data=json.dumps(self.test_artist),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        response = self.app.get('/api/artists/' + str(test_id))
        self.assertIsInstance(response.json, dict)
        self.assertEqual(200, response.status_code)
        self.assertEqual(creation_response.json.get('name'), response.json.get('name'))

        # Abnormal use case, expect 404 because of unexisting artist_id
        response = self.app.get('/api/artists/99999999')
        self.assertEqual(404, response.status_code)

    def test_update_artist(self):
        # Add test artist and catch the given id
        creation_response = self.app.post('/api/artists/', data=json.dumps(self.test_artist),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        random_name = str(uuid.uuid1())
        self.test_artist['name'] = random_name
        response = self.app.put('/api/artists/' + str(test_id), data=json.dumps(self.test_artist),
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(random_name, response.json.get('name'))

        # Abnormal use case, expect 409 because of duplicate artist name
        self.test_artist['name'] = 'LEN'  # todo Improve this hack
        response = self.app.put('/api/artists/' + str(test_id), data=json.dumps(self.test_artist),
                                content_type='application/json')
        self.assertEqual(409, response.status_code)

        # Abormal use case, expect 404 because of unexisting artist_id
        response = self.app.put('/api/artists/99999999', data=json.dumps(self.test_artist),
                                content_type='application/json')
        self.assertEqual(404, response.status_code)

    def test_delete_artist(self):
        # Add test artist and catch the given id
        creation_response = self.app.post('/api/artists/', data=json.dumps(self.test_artist),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        response = self.app.delete('/api/artists/' + str(test_id))
        assert 204 == response.status_code

        # Abnormal use case expect 404 because of unexisting artist_id
        response = self.app.delete('/api/artists/99999999')
        self.assertEqual(404, response.status_code)
