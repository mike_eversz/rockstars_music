import datetime
import json
import unittest
import uuid

from server import app


class TestSongEnpoint(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestSongEnpoint, self).__init__(*args, **kwargs)
        # TODO: Mock database and make cleaner (DRY)!

        _random_spotify_id = str(uuid.uuid1())[:22]
        self.test_song = {
            'name': 'Test song',
            'publication_year': 2020,
            'artist_id': 6,
            'shortname': 'dontfearthereaper',
            'bpm': 141,
            'duration': 322822,
            'genre': 'Metal',
            'spotify_id': _random_spotify_id,
            'album': 'Test album'
        }

        self.app = app.test_client()
        self.next_year = datetime.datetime.now().year + 1

    def test_create_song(self):
        # Normal use case
        response = self.app.post('/api/songs/', data=json.dumps(self.test_song), content_type='application/json')
        self.assertEqual(201, response.status_code)

        # Abnormal use case, expect 409 because of duplicate spotify_id
        response = self.app.post('/api/songs/', data=json.dumps(self.test_song), content_type='application/json')
        self.assertEqual(409, response.status_code)

        # Abnormal use case, expect 422 because of unexisting artist
        self.test_song['spotify_id'] = str(uuid.uuid1())[:22]
        self.test_song['artist_id'] = 999999
        response = self.app.post('/api/songs/', data=json.dumps(self.test_song), content_type='application/json')
        self.assertEqual(422, response.status_code)

        # Abnormal use case, expect 400 because of incorrect publication_year
        self.test_song['spotify_id'] = str(uuid.uuid1())[:22]
        self.test_song['publication_year'] = self.next_year
        response = self.app.post('/api/songs/', data=json.dumps(self.test_song), content_type='application/json')
        self.assertEqual(400, response.status_code)

    def test_get_all_songs(self):
        # Normal use case
        response = self.app.get('/api/songs/')
        self.assertIsInstance(response.json, list)
        self.assertEqual(200, response.status_code)

    def test_get_song(self):
        # Add test song and catch the given id
        creation_response = self.app.post('/api/songs/', data=json.dumps(self.test_song),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        response = self.app.get('/api/songs/' + str(test_id))
        self.assertIsInstance(response.json, dict)
        self.assertEqual(200, response.status_code)

        self.assertEqual(creation_response.json, response.json)

        # Abnormal use case, expect 404 because of unexisting song_id
        response = self.app.get('/api/songs/99999999')
        self.assertEqual(404, response.status_code)

    def test_update_song(self):
        # Add test song and catch the given id
        creation_response = self.app.post('/api/songs/', data=json.dumps(self.test_song),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        random_spotify_id = str(uuid.uuid1())[:22]
        self.test_song['spotify_id'] = random_spotify_id
        response = self.app.put('/api/songs/' + str(test_id), data=json.dumps(self.test_song),
                                content_type='application/json')
        self.assertEqual(200, response.status_code)
        self.assertEqual(random_spotify_id, response.json.get('spotify_id'))

        # Abnormal use case, expect 409 because of duplicate spotify id
        self.test_song['spotify_id'] = '5E4hUrxGYMecRnbr7SjLo6'  # todo Improve this hack
        response = self.app.put('/api/songs/' + str(test_id), data=json.dumps(self.test_song),
                                content_type='application/json')
        self.assertEqual(409, response.status_code)

        # Abnormal use case, expect 422 because of unexisting artist
        self.test_song['artist_id'] = 999999
        random_spotify_id = str(uuid.uuid1())[:22]
        self.test_song['spotify_id'] = random_spotify_id
        response = self.app.put('/api/songs/' + str(test_id), data=json.dumps(self.test_song),
                                content_type='application/json')
        self.assertEqual(422, response.status_code)

        # Abnormal use case, expect 400 because of incorrect publication_year
        self.test_song['spotify_id'] = str(uuid.uuid1())[:22]
        self.test_song['publication_year'] = self.next_year
        response = self.app.put('/api/songs/' + str(test_id), data=json.dumps(self.test_song), content_type='application/json')
        self.assertEqual(400, response.status_code)

    def test_delete_song(self):
        # Add test song and catch the given id
        creation_response = self.app.post('/api/songs/', data=json.dumps(self.test_song),
                                          content_type='application/json')
        test_id = creation_response.json.get('id')

        # Normal use case
        response = self.app.delete('/api/songs/' + str(test_id))
        assert 204 == response.status_code

        # Abnormal use case expect 404 because of unexisting song_id
        response = self.app.delete('/api/songs/99999999')
        self.assertEqual(404, response.status_code)
