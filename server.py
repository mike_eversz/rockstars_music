import os

from flask import Flask
from dotenv import load_dotenv

from app import blueprint
load_dotenv('app/main/.env')

app = Flask(__name__)
app.register_blueprint(blueprint)

if __name__ == "__main__":
    app.run(host=os.getenv('API_HOST'), port=os.getenv('API_PORT'), debug=os.getenv('API_DEBUG'))
